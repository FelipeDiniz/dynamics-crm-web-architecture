﻿using MSCRMWebApplication.Core.Interfaces;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace MSCRMWebApplication.Application.Repository
{
    public class EntityBaseServices<T> : IEntityBase<T> where T : Entity
    {
        protected CrmServiceClient _service;
        protected string logicalName = ((T)Activator.CreateInstance(typeof(T))).LogicalName;

        public EntityBaseServices()
        {
            _service = new CrmServiceClient(ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString);
        }

        public Guid Create(T entity)
        {
            try
            {
                return _service.Create(entity);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Delete(T entity)
        {
            try
            {
                _service.Delete(entity.LogicalName, entity.Id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                _service.Delete(logicalName, id);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public T Get(Guid id)
        {
            try
            {
                return (T)_service.Retrieve(logicalName, id, new ColumnSet(true));
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<T> GetAll(string stringSet)
        {
            try
            {
                QueryExpression qu = new QueryExpression();
                qu.EntityName = logicalName;
                qu.ColumnSet = new ColumnSet();

                string[] arrayFields = stringSet.Split(',');
                foreach (string i in arrayFields)
                    qu.ColumnSet.Columns.Add(i);

                qu.PageInfo = new PagingInfo();
                qu.PageInfo.Count = 5000;
                qu.PageInfo.PageNumber = 1;

                EntityCollection results = new EntityCollection();
                while (true)
                {
                    // Retrieve the page.
                    results = _service.RetrieveMultiple(qu);

                    // Check for more records, if it returns true.
                    if (results.MoreRecords)
                    {

                        // Increment the page number to retrieve the next page.
                        qu.PageInfo.PageNumber++;

                        // Set the paging cookie to the paging cookie returned from current results.
                        qu.PageInfo.PagingCookie = results.PagingCookie;
                    }
                    else
                    {
                        // If no more records are in the result nodes, exit the loop.
                        break;
                    }
                }

                return results.Entities.Select(e => e.ToEntity<T>()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<T> GetAll(string pageNumber, string stringSet)
        {
            try
            {
                QueryExpression qu = new QueryExpression();
                qu.EntityName = logicalName;
                qu.ColumnSet = new ColumnSet();

                string[] arrayFields = stringSet.Split(',');
                foreach (string i in arrayFields)
                    qu.ColumnSet.Columns.Add(i);

                qu.PageInfo = new PagingInfo();
                qu.PageInfo.Count = 50;
                qu.PageInfo.PageNumber = Convert.ToInt32(pageNumber);

                return _service.RetrieveMultiple(qu).Entities.Select(e => e.ToEntity<T>())
                        .ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(T entity)
        {
            try
            {
                _service.Update(entity);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<T> Search(string searchRegistration, string fieldSearch, string pageNumber, string stringSet)
        {
            try
            {
                QueryExpression qu = new QueryExpression();
                qu.EntityName = logicalName;
                string[] arrayFields = stringSet.Split('¢');
                foreach (string i in arrayFields)
                    qu.ColumnSet.Columns.Add(i);

                qu.Criteria.Conditions.Add(new ConditionExpression(fieldSearch, ConditionOperator.Like, searchRegistration));
                qu.PageInfo = new PagingInfo();
                qu.PageInfo.Count = 50;
                qu.PageInfo.PageNumber = Convert.ToInt32(pageNumber);

                return _service.RetrieveMultiple(qu).Entities.Select(e => e.ToEntity<T>()).ToList();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
