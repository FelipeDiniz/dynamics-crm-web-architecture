﻿using MSCRMWebApplication.Core.Entities;
using MSCRMWebApplication.Core.Interfaces;

namespace MSCRMWebApplication.Application.Repository
{
    public class IncidentServices : EntityBaseServices<Incident>, IIncident
    {
    }
}
