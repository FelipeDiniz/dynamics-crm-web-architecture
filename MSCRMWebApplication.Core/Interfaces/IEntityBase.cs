﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSCRMWebApplication.Core.Interfaces
{
    public interface IEntityBase<T> where T: Entity
    {
        Guid Create(T entity);
        void Update(T entity);
        void Delete(Guid id);
        void Delete(T entity);
        T Get(Guid id);
        List<T> GetAll(string stringSet);
        List<T> GetAll(string pageNumber, string stringSet);
        List<T> Search(string searchRegistration, string fieldSearch, string pageNumber, string stringSet);
    }
}
