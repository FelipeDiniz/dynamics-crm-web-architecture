﻿using CommonServiceLocator;
using MSCRMWebApplication.Core.Interfaces;
using MSCRMWebApplication.Application.Repository;
using SimpleInjector;

namespace MSCRMWebApplication.IOC
{
    public class Bindings
    {
        public static void Start(Container container)
        {
            //Infrastructure
            container.Register(typeof(IEntityBase<>), typeof(EntityBaseServices<>), Lifestyle.Scoped);
            container.Register<IIncident, IncidentServices>();

            //Locator
            ServiceLocator.SetLocatorProvider(() => new SimpleInjectorServiceLocatorAdapter(container)); 
        }

    }
}
