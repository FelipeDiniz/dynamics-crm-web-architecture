﻿using MSCRMWebApplication.Core.Interfaces;
using MSCRMWebApplication.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace MSCRMWebApplication.Controllers
{
    public class IncidentController : Controller
    {
        private readonly IIncident _incidentService;

        public IncidentController(IIncident incident)
        {
            _incidentService = incident;
        }


        // GET: Incident
        public ActionResult Index()
        {
            List<IncidentVM> incidents = _incidentService.GetAll("title,ticketnumber,customerid").Select(x =>
            new IncidentVM {
                title = x.Title,
                ticketnumber = x.TicketNumber,
                customerName = x.CustomerId.Name
            }).ToList();

            return View(incidents);
        }
    }
}