﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MSCRMWebApplication.ViewModels
{
    public class IncidentVM
    {
        public string title { get; set; }
        public string ticketnumber { get; set; }
        public string customerName { get; set; }
    }
}